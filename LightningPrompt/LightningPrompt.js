import { LightningElement, api } from 'lwc';

export default class LightningPrompt extends LightningElement {
    @api header;
    @api message;
    @api type = 'info';
    @api buttons;
    @api show;
    @api context;
    headerClass = 'slds-modal__header {0} slds-theme_alert-texture';

    connectedCallback() {
        if (this.type === 'info') {
            this.headerClass = this.headerClass.replace('{0}', 'slds-theme_info');
        } else if (this.type === 'error') {
            this.headerClass = this.headerClass.replace('{0}', 'slds-theme_error');
        } else if (this.type === 'warning') {
            this.headerClass = this.headerClass.replace('{0}', 'slds-theme_warning');
        } else if (this.type === 'offline') {
            this.headerClass = this.headerClass.replace('{0}', 'slds-theme_offline');
        }
    }

    renderedCallback() {
        if (this.show) {
            this.template.querySelector('.header-container').innerHTML = this.header;
            this.template.querySelector('.message-container').innerHTML = this.message;
        }
    }

    handleClick(event) {
        let button = event.target.dataset.id;
        this.dispatchEvent(new CustomEvent('promptaction', { detail: { type: this.type, button: button, context: this.context } }));
    }
}